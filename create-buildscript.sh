#!/bin/bash

set -e

scripts="
  build-gbp.sh
  build-gbp-maven-plugin.sh
  build-gbp-webext-plugin.sh
  build-gbp-python-plugin.sh
"

cat Title.nw Part1.nw Part2.nw Part3.nw Part4.nw Part5.nw Part6.nw > BuildWithGBPg.nw
t=`date +%c`

function build_script()
{
  notangle -R"$script" BuildWithGBPg.nw > "$script"
  sed --in-place \
    --expression='s/This is the end, my friend[[:cntrl:]]*/This is the end, my friend/' \
    "$1"
  echo "#generated on $t" >> "$1"
  chmod ugo+x "$1"
  bash -n "$1"
}

for script in $scripts; do
   build_script "$script"
done

# Remove auxillary file BuildWithGBPg.nw
rm BuildWithGBPg.nw
