% ===> this file was generated automatically by noweave --- better not edit it
% !TEX root = BuildWithGBP.nw.tex
\part{Overview} \chapter{License} 

The text of the book ``Building packages with Git-Buildpackage'' by Mechtilde und Michael Stehmann is licensed under the \index{License} \textbf{Creative Commons Attribution - ShareAlike 4.0 International License \index{Creative Commons} (CC BY-SA 4.0)}\cite{CreativeCommon40-2013}.

The included code is available under the terms of the \textbf{GNU General Public License Version 3} \index{GNU General Public License} or (at your option) any later version\cite{GPLv3}.

Copyright: \textsuperscript{\textcopyright} 2012-2023 Mechtilde Stehmann (Email: mechtilde@debian.org), \newline Michael Stehmann (Email: michael@canzeley.de)

\chapter{Who should read this book?} 

The information in this book is of particular interest to users of the script. But also people who are generally interested in packaging \index{Packaging} for a distribution will find information in this book.

This book does not want to be a ``textbook'' for building \texttt{Debian} packages. It is more of an experience report, where the experiences have been ``molded into code''.

The book describes how  \texttt{Debian} packages are created using a git repository with the programs from the package  \textit{git-buildpackage} \cite{Guenther2006-2017} and other useful commands. Afterwards, the reader should be able to use the program as shown and the descriptions of the individual steps to build ``production quality'' \texttt{Debian} packages.

The program script itself does \textbf{not} build \texttt{Debian} packages, but assists the user in building them. It is only an assistance program. 

This book can also be used to understand problems that can arise when packaging \texttt{Debian} packages.

Packaging is basically not difficult, though there are always new challenges. Packaging is therefore fun.

\chapter{How is this book conceived?} \section{Motivation} 

What is driving us to write such a book?

To understand this, you need to know the following::

Packaging involves executing many commands in a meaningful order at the shell. In addition, many small files must be maintained and included. The smallest errors and inaccuracies usually result in the package not being built correctly. It's also time-consuming and error-prone to keep putting in the correct options.

To keep these sources of error as small as possible, these steps were combined in a shell script. In the course of the time and with each further package this script grows and is refined also always further. So far, this has already become an extensive program script.

When Mechtilde started to deal with building \texttt{Debian} packages, the question was how to document and automate these many steps. From the beginning need for documentation could not be met with comments, neither in the individual files nor in this program script.

That's why we started early to record our packaging steps in detail. We paid special attention to descriptions that make decisions traceable and verifiable. This makes it easier to make necessary changes.

For this reason, we have also chosen the long form as far as possible when specifying options. This facilitates readability.

So the documentation should describe the actual packaging as well as explain the script.

The \texttt{Debian} distribution \index{Distribution} is the work of many people. It consists of tens of thousands of packages. Building the packages is a major task of the package maintainers. Many package maintainers use their own scripts for this purpose. Publishing such a script is therefore a risk. If our script makes life easier for some package maintainers and introduces newcomers to package building, this venture has been worthwhile.

The described program script does not refer to a specific  \texttt{Debian} package. Rather, it is intended to be used to build simple  \texttt{Debian} packages in general.

It describes the steps we need to take to package the packages maintained by Mechtilde. The program script does not claim that it can be used to build a \texttt{Debian} package from any source code.

In many places, you can and must also intervene manually. The description of the processes during packaging should be of help here.

The fact that the program script presupposes the possibility of manual intervention makes it necessary for the program script to repeatedly check whether the prerequisites that the authors have assumed are actually present. This necessity unfortunately increases the size and complexity of the script and thus also the size of the book.

\section{Under contruction} 

The book and the script are still ``under development'', because new experiences keep flowing in.

The book is originally written in German, the program script interface in English. Localizations are welcome. As a ``proof-of-concept'', a part of the book has already been translated into English. \footnote{\url{https://ddp-team.pages.debian.net/dpb/BuildWithGBP.pdf}}

The release \index{Publishing} of the source code \index{Source code} is done in the \texttt{Git} repository\footnote{\url{https://salsa.debian.org/ddp-team/dpb}} provided by the \texttt{Debian} project. There, the \textit{CI/CD} (chapter \ref{sec:gitlab-ci.yml}, page \pageref{sec:gitlab-ci.yml}) is enabled. Therefore, the book is available as \textit{*.pdf}\footnote{\url{https://ddp-team.pages.debian.net/dpb/BuildWithGBP.pdf}} and \textit{*.epub}\footnote{\url{https://ddp-team.pages.debian.net/dpb/BuildWithGBP.epub}}. There you can also download the program scripts.\footnote{\url{https://ddp-team.pages.debian.net/dpb/build-gbp.sh} \newline

\url{https://ddp-team.pages.debian.net/dpb/build-gbp-maven-plugin.sh}\newline \url{https://ddp-team.pages.debian.net/dpb/build-gbp-webext-plugin.sh}\newline \url{https://ddp-team.pages.debian.net/dpb/build-gbp-python-plugin.sh}\newline \url{https://ddp-team.pages.debian.net/dpb/build-gbp-java-plugin.sh} }

\section{Tools} ``Living more comfortably with documentation'' is a common phrase in our \textit{peer group}.

Which \index{Tools} tools can be used to create such documentation? Are these tools also available as \texttt{Debian} packages?

Mechtilde received an important hint about this at an event for the \texttt{Software Freedom Day 2012} in Cologne. There she learned about the possibilities of \texttt{noweb} \cite{NoWeb2018}\footnote{s.a. \url{https://en.wikipedia.org/wiki/Noweb}} \index{noweb} This also meant getting closer to \LaTeX{} \index{\LaTeX{}}.

In this combination, it is possible to maintain code and its description in \emph{one} document. Donald Knuth refers to this as \index{Literate Programming} ``Literate Programming'' \footnote{\url{http://www.literateprogramming.com/}}

Further we have dealt with the fact that \LaTeX\index{PDF Document}{} can be used to create documents in  EPUB format in addition to PDF documents\texttt{}\index{EPUB Document}. These documents can also be read with an e-book reader\index{ebook}. (chapter~\ref{sec:CreateBook}, page~\pageref{sec:CreateBook})

Translating this book into another language is a special challenge. Our tests have shown that \texttt{OmegaT} \footnote{\url{https://packages.debian.org/sid/omegat}} is a useful and convenient tool for that purpose. The corresponding process is also documented in a separate booklet \footnote{\url{https://oldmike.pages.debian.net/omegatbooklet/omegat.pdf}}.

The bibliography is created and maintained with \textit{jabref}. The file created in this way can be included into the \LaTeX{} document.

The used editor is \textit{geany} \index{Geany} with the plugin \textit{geany-plugin-latex}t.

\textit{Git} \index{git} is ingenious. Building is therefore done with the tools from the \index{git-buildpackage} \textit{git-buildpackage}\footnote{\url{https://packages.debian.org/sid/git-buildpackage}} package.

The people at \texttt{Debian} have created many useful programs that make building \texttt{Debian} packages easier and more uniform. The program script presented was therefore created ``on the shoulders of giants''.

It is used to call up the auxiliary programs used in an expedient order and to provide them with useful options. It should show its users the way and make their work easier. To facilitate its adaptation to the needs of its users, it is a shell script.

\chapter{Conventions} 

Some notes for a better understanding of the book:

\section{System} 

The book and especially the program script were created on a \textit{64-Bit-PC} architecture. This is called \textit{amd64} in \texttt{Debian}. Another name for this system is \textit{x86-64}.

\section{Terminology} 

A \textbf{new package} is a package, which the program script does not know yet. I.e. no configuration file exists.

A \textbf{new version } is a new upstream version. Building a new version is followed by building a new revision.

A \textbf{new revision} denotes a new \texttt{Debian} package to be uploaded.

\section{Typography} All program names are set in \textit{italic}.All proper names are set in \texttt{non\-proportional} type. Superscript numbers point to footnotes on the same page. Citations to an entire document point directly to the bibliography in square brackets [~].

All shell command options are given in the long form, as far as this is possible. This increases the readability.

For the abbreviations used, please refer to the entries in the glossary\footnote{\url{https://wiki.debian.org/Glossary}}.

\section{Source Code Representation} 

The source code is presented in segments (so-called code chunks). The order of these code chunks in the book often does not correspond to the order in the scripts. The fact that the order in the book and script need not match is an advantage of \texttt{noweb}.

% !TEX root = BuildWithGBP.nw
\chapter{Quick Guide} 

This section \index{Quick Guide} is intended to help the user understand the steps required and find their description more quickly in the book. 

\section{Preparing the build environment} 

The following steps are necessary until there is a \texttt{Debian} package of the book and scripts. \\ 

\begin{enumerate} \item Downloading from \textit{salsa.debian.org} The source code of this book and the following two scripts can be found at \textit{\url{https://salsa.debian.org/ddp-team/dpb}}. \item Generate the PDF with \textit{./create-book.sh} (chapter \ref{sec:CreateBook}, page \pageref{sec:CreateBook}) \item Generate the program script with \textit{./create-buildscript.sh} (chapter \ref{sec:CreateBuildscript}, page \pageref{sec:CreateBuildscript}). \item \textbf{Alternatively:} Download the files from \begin{itemize} \item{\url{https://ddp-team.pages.debian.net/dpb/en_US/BuildWithGBP.pdf}} \item{\url{https://ddp-team.pages.debian.net/dpb/create-book.sh}} \item{\url{https://ddp-team.pages.debian. net/dpb/create-buildscript.sh}} \end{itemize} \item Creating symlinks to the generated scripts under \textit{/usr/local/bin} This way the current scripts are also in the \textit{PATH} and can be called from any point in the file system. \item Install all dependencies that the program script requires. (Chapter \ref{sec:scriptdependency}, page \pageref{sec:scriptdependency}) \item Creating the required directories (Chapter \ref{sec:FolderStructure}, page \pageref{sec:FolderStructure}). \end{enumerate} 

\section{Using the program scripts} 

\begin{enumerate} \item Provide the GPG key to sign the git tag \item Run \textit{build-gbp.sh} (chapter \ref{sec:Hauptprogramm}, page \pageref{sec:Hauptprogramm}) \item Use the project name to create or load the configuration file (chapter \ref{sec:ProjektnamenAbfragen}, page \pageref{sec:ProjektnamenAbfragen}).

%\item Bauen eines neuen \texttt{Debian}-Paketes
%(Kapitel~\ref{chap:BuildNewVersion}, Seite~\pageref{chap:BuildNewVersion})
\item Obtain the upstream source code (chapter \ref{chap:BuildNewVersion}, page \pageref{chap:BuildNewVersion}) \item Create or. Update the files in the \textit{debian/} directory (chapter \ref{chap:NeueRevisionBauen}, page \pageref{chap:NeueRevisionBauen}) \item Make changes to the source code (chapter \ref{chap:Aenderungen_am_Upstream_Code_vornehmen}, page \pageref{chap:Aenderungen_am_Upstream_Code_vornehmen}) \item Build the \texttt{Debian} package (chapter \ref{chap:Bauen}, Page \pageref{chap:Bauen}) \item Checking the quality of the built \texttt{Debian} package (Chapter \ref{chap:Ueberpruefungen}, Page \pageref{chap:Ueberpruefungen}) \item Publishing the \texttt{Debian} package (Chapter \ref{chap:Uploading}, Page \pageref{chap:Uploading}). \end{enumerate} 

The program script is modular, so that you can `get out' at many points and `get back in' later. It also allows manual intervention.  


Before working with the program script, you should also take a look at the following part.

\nwfilename{Part1.nw} 